# Docker Image "composer-deploy"
PHP-Image (php:7.2) with installed composer + deployment tools (git, rsync, ssh-agent)

## usage
you can use this image from the gitlab docker registry:
`registry.gitlab.com/skanto/docker-composer-deploy:latest`

## development
Every push to the Master-Branch of `gitlab.com/skanto/docker-composer-deploy.git` will trigger ci/cd workflow.
The ci/cd workflow will build the new docker-image, mark it as "latest" and push it to the registry at gitlab.com
